FROM tiangolo/uvicorn-gunicorn:python3.7-alpine3.8
LABEL maintainer="euri10 <benoit.barthelet@gmail.com>"

RUN apk update && \
    apk add --no-cache postgresql-libs && \
    apk add --no-cache --virtual .build-deps \
    gcc \
    libc-dev \
    make \
    build-base \
    linux-headers \
    #postgres
    postgresql-dev \
    musl-dev \
    # argon
    libffi-dev
    # emails
#    libxml2-dev \
#    libxslt-dev

ARG env=prod
RUN ash -c "if [ $env == 'dev' ] ; then pip install jupyter ; fi"
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN apk --purge del .build-deps

RUN rm /app/main.py /gunicorn_conf.py
COPY ./app /app
#CMD ["ash", "-c", "while true; do sleep 1; done"]
