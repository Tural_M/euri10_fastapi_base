import json
import multiprocessing
import os

from applog.utils import read_logging_config

max_workers = os.getenv("MAX_WORKERS", "2")
workers_per_core = os.getenv("WORKERS_PER_CORE", "1")
web_concurrency = os.getenv("WEB_CONCURRENCY", None)
host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("PORT", "8000")
bind_env = os.getenv("BIND", None)

if bind_env:
    use_bind = bind_env
else:
    use_bind = f"{host}:{port}"

cores = multiprocessing.cpu_count()
workers_per_core_float = float(workers_per_core)
default_web_concurrency = workers_per_core_float * cores
if web_concurrency:
    web_concurrency_int = int(web_concurrency)
    assert web_concurrency_int > 0
else:
    web_concurrency_int = min(int(max_workers), int(default_web_concurrency))

# Gunicorn config variables
workers = web_concurrency_int
bind = use_bind
keepalive = 120
errorlog = "-"
logconfig_dict = read_logging_config("applog/logging.yml")

# For debugging and testing
log_data = {
    "logconfig_dict": logconfig_dict,
    "workers": workers,
    "bind": bind,
    # Additional, non-gunicorn variables
    "workers_per_core": workers_per_core,
    "host": host,
    "port": port,
}
print(json.dumps(log_data))
