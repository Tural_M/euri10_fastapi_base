from main import app
from worker.worker import celery1


def test_celery1_url(client):
    data = {"msg": "test"}
    url = app.url_path_for("celery1")
    response = client.post(url, json=data)
    assert response.status_code == 201
    assert response.json().get("msg") == "Word received"


def test_celery1():
    task = celery1.s("worD").apply()
    assert task.status == "SUCCESS"
    assert task.result == "celery1 task return worD"
