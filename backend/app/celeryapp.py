from __future__ import absolute_import

from celery import Celery

import settings

celery_app = Celery(
    "mycelery",
    broker=f"amqp://{settings.RABBITMQ_DEFAULT_USER}:{settings.RABBITMQ_DEFAULT_PASS}@rabbitmq:5672",
    backend="rpc://",
)

celery_app.conf.update(
    {
        "task_routes": {
            "worker.worker.celery1": {"queue": "main-queue"},
            "worker.worker.celery2": {"queue": "beat-queue"},
        }
    }
)
