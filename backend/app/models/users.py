from datetime import datetime
from typing import List, Optional

import sqlalchemy
from pydantic import BaseModel, EmailStr
from sqlalchemy import func, text
from sqlalchemy.dialects.postgresql import UUID

from models import metadata

users = sqlalchemy.Table(
    "users",
    metadata,
    sqlalchemy.Column(
        "id",
        UUID(),
        server_default=text("uuid_generate_v4()"),
        primary_key=True,
        unique=True,
    ),
    sqlalchemy.Column("name", sqlalchemy.String, nullable=False, unique=True),
    sqlalchemy.Column("email", sqlalchemy.String, nullable=False, unique=True),
    sqlalchemy.Column("password", sqlalchemy.String, nullable=False),
    sqlalchemy.Column(
        "time_created", sqlalchemy.DateTime(timezone=True), server_default=func.now()
    ),
    sqlalchemy.Column(
        "time_updated", sqlalchemy.DateTime(timezone=True), onupdate=func.now()
    ),
)


class UserIn(BaseModel):
    name: str
    email: EmailStr
    password: str


class UserOut(BaseModel):
    name: str
    email: EmailStr
    time_created: datetime
    time_updated: Optional[datetime]


class UserPatch(BaseModel):
    name: Optional[str] = None
    email: Optional[EmailStr] = None
    password: Optional[str] = None


class UserListPaginated(BaseModel):
    users: List[UserOut]
    next_page_token: Optional[str]
