from datetime import datetime

from celery.schedules import crontab

from celeryapp import celery_app


@celery_app.task(acks_late=True)
def celery1(word: str):
    return f"celery1 task return {word}"


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('world') every 30 seconds
    # sender.add_periodic_task(30.0, celery2.s("world"), expires=10)

    # Executes every 15min
    sender.add_periodic_task(
        crontab(minute="*/15"),
        celery2.s(f"Beat from celery worker, it's {str(datetime.utcnow())}"),
    )


@celery_app.task
def celery2(arg):
    print(arg)
